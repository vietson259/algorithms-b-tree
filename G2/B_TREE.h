﻿#pragma once
#include <iostream>
#include <string>

using namespace std;

#define RANK 37

void copyStr(char dest[26], const char src[26]) {
	int i = 0;
	for (i = 0; i < strlen(src); i++)
		dest[i] = src[i];
	dest[i] = '\0';
}

//******************************
//dữ liệu sữ dụng cho từ điển
class Data {
public:
	char word_[26]; //1 từ có nhiều nhất 25 ký tự //26 byte

	Data() {}

	Data(const Data& src) {
		copyStr(word_, src.word_);
	}

	Data(char* str) {
		strcpy_s(word_, (strlen(str) + 1) * sizeof(char), str);
	}

	~Data() {}

	int datacmp(const Data& data1) {
		return strcmp(this->word_, data1.word_);
	}

	Data& operator= (const Data& src) {
		copyStr(word_, src.word_);
		return *this;
	}
};

template <class T>
int seachIndex(T* key, int size, T src) {
	for (int i = 0; i < size; i++) {
		if (key[i].datacmp(src) == 1)
			return i;
	}
	return size;
}

template <class T>
int seachIndexCheck(T* key, int size, T src) {
	//kiem tra key dau
	if (key[0].datacmp(src) == 1)
		return 0;
	//kiem tra key cuoi
	if (key[size - 1].datacmp(src) == 0)
		return -1;
	for (int i = 0; i < size - 1; i++) {
		if (key[i].datacmp(src) == 0)
			return -1;
		if (key[i].datacmp(src) == -1 && key[i + 1].datacmp(src) == 1)
			return i + 1;
	}
	return size;
}

//***********************************
//***********************************
//cấu trúc node trong b - cây
template <class T>
class BNode {
private:

	void addKeyToEmptyNode(const T& key);

	void moveLeft();

	T splitNode(BNode<T>* &newNode);

	int NodeCmp(BNode<T>* node);

	char unused[38]; //14 byte không sử dụng => tổng số byte của node là 1024 = 2 * 512
public:
	//**************************************************************************
	//**************************************************************************
	//cấu trúc dữ liệu của 1 node trong B cây
	unsigned short rank_ = RANK; //bậc của cây
	bool leaf_ = true; // true: là lá; false: ngược lại
	bool empty_; // false: nút đầy true: nút còn trống
	T key_[RANK];	//mảng các key //1 node có m - 1 key, ta cấp phát dư 1 key
	unsigned short NOfKey_;		// số lượng key hiện có trong 1 node
	BNode** branch_;  // mảng các con trỏ trỏ tới các cây con
	BNode* nodePar_; //node cha
	BNode* brotherLeft; //nút anh em trái
	BNode* brotherRight; //nút anh em phải
	//**************************************************************************

	BNode();

	BNode(unsigned short rank);

	~BNode();

	//phần tử lớn nhất
	T elementMax();

	//phần tử nhỏ nhất
	T elementMin();

	void addKeyToCurBNode(const T& key);

	BNode<T>& operator=(const BNode<T>& cay) {
		for (int i = 0; i < rank_; i++)
			copyStr(this->key_[i], cay.key_[i]);
	}

	bool isWordInNode(string word) {
		for (int i = 0; i < this->NOfKey_; i++) {
			if (strcmp(this->key_[i].word_, word.c_str()) == 0)
				return true;
		}
		return false;
	}
};

template <class T>
void BNode<T>::addKeyToEmptyNode(const T& key) {
	this->key_[this->NOfKey_] = key;
	for (int i = NOfKey_ - 1, j = NOfKey_; i >= 0; i--, j--) {
		if (key_[j].datacmp(this->key_[i]) == 1)
			break;
		else
			swap(key_[j], key_[i]);
	}
	this->NOfKey_ += 1;
}

template <class T>
void BNode<T>::moveLeft() {
	int last = this->nodePar_->NOfKey_;
	//BNode<T>** braTemp = new BNode<T>*[RANK];
	BNode<T>* datTemp = this->nodePar_->branch_[0];
	while (datTemp->brotherLeft != NULL) {
		if (datTemp->nodePar_ != datTemp->brotherLeft->nodePar_)
			break;
		else
			datTemp = datTemp->brotherLeft; //lấy con trỏ nhánh trái nhất nhưng phải cùng cha
	}
	int i = 0;
	while (datTemp != NULL) {
		if (datTemp->brotherRight == NULL || datTemp->nodePar_ != datTemp->brotherRight->nodePar_) {
			break;
		}
		else {
			this->nodePar_->branch_[i] = datTemp;
			i++;
			datTemp = datTemp->brotherRight;
		}
	}
	this->nodePar_->branch_[i] = datTemp;
}

template <class T>
T BNode<T>::splitNode(BNode<T>* &newNode) {
	//xác định vị trí chia đôi
	int midKey = -1;//vị trí chia đôi
	(rank_ % 2 != 0) ? midKey = rank_ / 2 : midKey = rank_ / 2 - 1;
	T keyMid = this->key_[midKey];
	//cập nhật các giá trị khác
	newNode->leaf_ = this->leaf_; newNode->rank_ = this->rank_;
	newNode->empty_ = true; newNode->NOfKey_ = rank_ - midKey - 1;
	//cập nhật giá trị con trỏ anh em
	if (this->brotherRight != NULL)
		this->brotherRight->brotherLeft = newNode;
	newNode->brotherRight = this->brotherRight;
	newNode->brotherLeft = this;
	//sao chép các key qua newNode, toán tử = không làm được
	this->brotherRight = newNode;
	//sao chép mảng key
	for (int i = midKey + 1, j = 0; i < rank_; i++, j++) {
		Data* datTemp = new Data(this->key_[i]);
		newNode->key_[j] = *datTemp;
		delete datTemp;
	}
	//sao chép mảng branch vào node mới
	if (this->branch_[0] != NULL)
	for (int i = midKey + 1, j = 0; i <= this->NOfKey_; i++, j++)
		newNode->branch_[j] = this->branch_[i];
	//điều chỉnh lại node hiện tại
	this->NOfKey_ = midKey; this->empty_ = true;
	for (int i = midKey; i < rank_; i++) {
		char null[26] = "";
		copyStr(this->key_[i].word_, null);
	}
	for (int i = midKey + 1, j = 0; i <= rank_; i++, j++)
		this->branch_[i] = NULL;
	//điều chỉnh quan hệ cha con
	newNode->nodePar_ = this->nodePar_;
	if (!this->leaf_) {
		for (int i = 0; i <= this->NOfKey_; i++)
			this->branch_[i]->nodePar_ = this;
		for (int j = 0; j <= newNode->NOfKey_; j++)
			newNode->branch_[j]->nodePar_ = newNode;
	}
	return keyMid;
}

template <class T>
int BNode<T>::NodeCmp(BNode<T>* node) {
	return this->elementMin().datacmp(node->elementMin());
}

template <class T>
BNode<T>::BNode() { //hàm dựng mặc định
	branch_ = new BNode*[RANK]; //mặc định b - cây có bậc m = 23
	NOfKey_ = 0; //mặc định chưa có key nào trong node
	empty_ = true; //mặc định nút rỗng
	brotherRight = brotherLeft = nodePar_ = NULL; //mặc định chưa có anh em
}

template <class T>
BNode<T>::BNode(unsigned short rank) { //hàm dựng có tham số rank chính bằng bậc của b - cây
	rank_ = rank;
	branch_ = new BNode*[rank]; //mặc định b - cây có bậc m = 38
	NOfKey_ = 0; //mặc định chưa có key nào trong node
	empty_ = true;
	brotherRight = brotherLeft = nodePar_ = NULL;
}

template <class T>
BNode<T>::~BNode() {
	if (this->branch_ != NULL) {
		delete[] branch_;
		branch_ = NULL;
	}
}

//phần tử lớn nhất
template <class T>
T BNode<T>::elementMax() {
	int index = 0;
	(NOfKey_ > 0) ? index = NOfKey_ - 1 : index = 0;
	return key_[index];
}

//phần tử nhỏ nhất
template <class T>
T BNode<T>::elementMin() {
	return key_[0];
}

template <class T>
void BNode<T>::addKeyToCurBNode(const T& key) {
	if (this->empty_) {
		this->addKeyToEmptyNode(key);
		if (this->NOfKey_ == rank_ - 1)
			this->empty_ = false;
		return;
	}
	else {
		addKeyToEmptyNode(key);
		BNode<T>* newNode = new BNode<T>(rank_);
		T midKey = this->splitNode(newNode); //trả về key giữa
		if (this->nodePar_ != NULL) {
			this->nodePar_->branch_[this->nodePar_->NOfKey_ + 1] = newNode;
			newNode->moveLeft();
			this->nodePar_->addKeyToCurBNode(midKey);
			return;
		}
		else {
			BNode<T>* newPar = new BNode<T>(this->rank_);
			newPar->leaf_ = false;
			newPar->branch_[0] = this;
			newPar->branch_[1] = newNode;
			this->nodePar_ = newPar;
			newNode->nodePar_ = newPar;
			newPar->addKeyToEmptyNode(midKey);
		}
	}
}

//tính 1 hàng có bao nhiêu node
template<class T>
int count(const BNode<T>* temp, int& minKey, int& maxKey) {
	BNode<T>* next = NULL;
	int i = 0;
	while ((next = temp->brotherRight) != NULL) {
		if (minKey > next->NOfKey_)
			minKey = next->NOfKey_;
		if (maxKey < next->NOfKey_)
			maxKey = next->NOfKey_;
		i++;
		temp = next;
	}
	return i + 1;
}

//**********************************************
//**********************************************
//Cấu trúc B cây
template <class T>
class BTree {
	unsigned short rank_ = RANK; //bậc của b - cây mặc định 38
	
	BNode<T>* seachNodeToAdd(const T& key); //tìm node có thể thêm

public:
	BNode<T>* roof_; //nút gốc của cây

	BTree();

	BTree(unsigned short rank);

	~BTree();

	void addKey(const T& key);

	int countNode(int& minKey, int& maxKey) {
		int sum = 0;
		BNode<T>* node = this->roof_;
		minKey = maxKey = this->roof_->NOfKey_;
		while (!node->leaf_) {
			sum += count(node, minKey, maxKey);
			node = node->branch_[0];
		}
		sum += count(node, minKey, maxKey);
		return sum;
	}

	int countNodeLeaf(int& height) {
		height = 0;
		int sum = 0;
		BNode<T>* node = this->roof_;
		while (!node->leaf_) {
			height += 1;
			node = node->branch_[0];
		}
		height += 1;
		while (node != NULL) {
			sum += 1;
			node = node->brotherRight;
		}
		return sum;
	}

	bool laCoLoi(const T& key, string& find) {
		find = "R";
		BNode<T>* temp = this->roof_;
		while (!temp->leaf_) {
			int i = seachIndexCheck(temp->key_, temp->NOfKey_, key);
			if (i == -1)
				return false;
			temp = temp->branch_[i];
			char c[3];
			_itoa_s(i, c, 10);
			find = find + " " + c;
		}
		string k = key.word_;
		if (!temp->isWordInNode(k))
			return true;
		return false;
	}
};

template <class T>
BNode<T>* BTree<T>::seachNodeToAdd(const T& key) {
	BNode<T>* temp = this->roof_;
	if (temp->leaf_)
		return temp;
	while (!temp->leaf_) {
		int i = seachIndex(temp->key_, temp->NOfKey_, key);
		temp = temp->branch_[i];
	}
	return temp;
}

template <class T>
void BTree<T>::addKey(const T& key) {
	BNode<T>* nodeAdd = seachNodeToAdd(key);
	nodeAdd->addKeyToCurBNode(key);
	//cập nhật lại roof
	BNode<T>* temp = nodeAdd;
	while (temp->nodePar_ != NULL) {
		temp = temp->nodePar_;
		this->roof_ = temp;
	}
}

template <class T>
BTree<T>::BTree() {
	roof_ = new BNode<T>(rank_);
}

template <class T>
BTree<T>::BTree(unsigned short rank) {
	roof_ = new BNode<T>(rank);
	rank_ = rank;
}

template <class T>
BTree<T>::~BTree() {

}





//*****************************
//*****************************
//xử lý file
#include <vector>
#include <fstream>

template <class T>
void ketQua(string ten, BTree<T>& tree, vector<string>& spell) {
	ofstream ghi(ten);
	ghi << "------Tree info: \n";
	int minKey, maxKey, height;
	int n = tree.countNode(minKey, maxKey);
	ghi << "+ Number of nodes : " << n << "\n";
	BNode<Data> node;
	ghi << "+ Size of each node : " << sizeof(node) << "\n";
	ghi << "+ Max keys in each node : " << maxKey << "\n";
	ghi << "+ Min keys in each node : " << minKey << "\n";
	ghi << "+ Average fulfill(%) : " << (float)235886 * 100 / (n * (RANK - 1)) << "%" << "\n";
	ghi << "+ Number of leaf - nodes : " << tree.countNodeLeaf(height) << "\n";
	ghi << "+ Height of tree : " << height << "\n";
	ghi << "------Spelling result: ";
	for (int i = 0; i < spell.size(); i++) {
		string find;
		char c[26];
		strcpy_s(c, 26, spell[i].c_str());
		Data dat(c);
		if (tree.laCoLoi(dat, find)) {
			ghi << "\n+ " << "<" << spell[i] << ">: ";
			ghi << find;
		}
	}
	ghi.close();
}

int loading(string ten, BTree<Data>& cay) {
	ifstream doc;
	doc.open(ten);
	Data dat;
	int i = 0;
	while (doc >> dat.word_) {
		cay.addKey(dat);
		i++;
	}
	doc.close();
	return i;
}


void loadText(string ten, vector<string>& docu) {
	ifstream doc;
	doc.open(ten);
	string temp = "";
	char c;
	while (doc.get(c)) {
		if (c != '.' && c != ',' && c != ' ' && c != '\n')
			temp += c;
		else {
			if (temp != "")
				docu.push_back(temp);
			temp = "";
		}
	}
}

